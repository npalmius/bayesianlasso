function [h_subplots] = plot_coefficient_samples(beta_samples, y_bar_samples, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Sep-2016

    if ~get_argument({'show_figures', 'showfigures', 'show_figure', 'showfigure'}, true, varargin{:});
        return
    end
    
    latex_labels = get_argument({'latexlabels', 'latex_labels'}, false, varargin{:});
    latex_ticks = get_argument({'latexticks', 'latex_ticks'}, latex_labels, varargin{:});
    times_labels = get_argument({'timeslabels', 'times_labels'}, false, varargin{:});
    times_ticks = get_argument({'timesticks', 'times_ticks'}, times_labels, varargin{:});
    
    label_opts = {};
    if latex_labels
        label_opts = {'Interpreter', 'latex'};
    elseif times_labels
        label_opts = {'FontName', 'Times New Roman', 'Interpreter', 'none'};
    end
    
    tick_label_opts = {};
    if latex_ticks
        tick_label_opts = {'TickLabelInterpreter', 'latex'};
    elseif times_ticks
        tick_label_opts = {'FontName', 'Times New Roman'};
    end
    
    feature_labels = get_argument({'featurelabels', 'feature_labels'}, {}, varargin{:});
    offset_label = get_argument({'offsetlabel', 'offset_label'}, '$\beta_0$', varargin{:});

    burnin = ceil(size(beta_samples, 2) * 0.2) + 1;
    
    if size(y_bar_samples, 2) > 1
        n = size(y_bar_samples, 1) - burnin + 1;
        y_bar_all_samples = NaN(size(y_bar_samples, 2) * n, 1);
        for i = 1:size(y_bar_samples, 2)
            y_bar_all_samples((((i - 1) * n) + 1):(i * n)) = y_bar_samples(burnin:end, i);
        end
    else
        y_bar_all_samples = y_bar_samples(burnin:end);
    end
    
    if size(beta_samples, 3) > 1
        n = size(beta_samples, 2) - burnin + 1;
        beta_all_samples = NaN(size(beta_samples, 1), size(beta_samples, 3) * n);
        for i = 1:size(beta_samples, 3)
            beta_all_samples(:, (((i - 1) * n) + 1):(i * n)) = beta_samples(:, burnin:end, i);
        end
    else
        beta_all_samples = beta_samples(:, burnin:end);
    end
    
    histogram_bars = get_argument({'histogram_bars', 'histogrambars'}, 199, varargin{:});
    beta_max_min = max([ceil(max(max(beta_all_samples))) -floor(min(min(beta_all_samples)))]);
    beta_max = beta_max_min;
    beta_max = get_argument({'beta_max', 'betamax'}, beta_max, varargin{:});
    beta_min = -beta_max_min;
    beta_min = get_argument({'beta_min', 'betamin'}, beta_min, varargin{:});
    beta_range = linspace(beta_min, beta_max, histogram_bars + 1);
    y_bar_max = get_argument({'y_bar_max', 'ybarmax'}, ceil(max(y_bar_all_samples)), varargin{:});
    y_bar_min = get_argument({'y_bar_min', 'ybarmin'}, floor(min(y_bar_all_samples)), varargin{:});
    y_bar_range = linspace(y_bar_min, y_bar_max, histogram_bars + 1);
    beta_len = size(beta_all_samples, 1);
    
    coefficient_label_prefix = '  ';
    if latex_labels
        coefficient_label_prefix = '~~';
    end
    
    h = cell(beta_len + 1, 1);
    y_max = 0;
    
    for m = 1:beta_len
        h{m} = subplot(beta_len + 2, 1, m);
        histogram(beta_all_samples(m, :), beta_range);
        
        if m < beta_len
            set(gca, 'xticklabel', []);
        end
        pos = get(gca, 'pos');
        pos_diff = pos(4) * 0.2;
        pos(4) = pos(4) + pos_diff;
        pos(2) = pos(2) - pos_diff * ((beta_len - (m - 1) - 1) / (beta_len - 1));
        set(gca, 'pos', pos);
        
        set(gca, 'xlim', [beta_min beta_max]);
        ylim = get(gca, 'ylim');
        y_max = max([y_max ylim(2)]);
        
        if isempty(feature_labels)
            text(0, 0.5, sprintf('~~$\\beta_{%i}$', m), 'Units', 'normalized', 'Interpreter', 'latex')
        else
            text(0, 0.5, sprintf('%s%s', coefficient_label_prefix, feature_labels{m}), 'Units', 'normalized', label_opts{:})
        end
        
        if numel(tick_label_opts) > 0
            set(gca, tick_label_opts{:})
        end
    end
    
    h{beta_len + 1} = subplot(beta_len + 2, 1, beta_len + 2);
    histogram(y_bar_all_samples, y_bar_range);

    pos = get(gca, 'pos');
    pos_diff = pos(4) * 0.2;
    pos(4) = pos(4) + pos_diff;
    set(gca, 'pos', pos);

    set(gca, 'xlim', [y_bar_min y_bar_max]);
    ylim = get(gca, 'ylim');
    y_bar_max = ylim(2);
    
    if isempty(feature_labels) || latex_labels
        text(0, 0.5, sprintf('~~%s', offset_label), 'Units', 'normalized', 'Interpreter', 'latex')
    else
        text(0, 0.5, sprintf('~\\,%s', offset_label), 'Units', 'normalized', 'Interpreter', 'latex')
    end

    if numel(tick_label_opts) > 0
        set(gca, tick_label_opts{:})
    end

    for m = 1:(beta_len + 1)
        if m <= beta_len
            this_y_max = y_max;
        else
            this_y_max = y_bar_max;
        end
        set(h{m}, 'ylim', [0 this_y_max]);
        set(h{m}, 'ytick', [0 (this_y_max / 2) this_y_max]);
        this_label = get(h{m}, 'yticklabel');
        this_label{1} = '';
        this_label{2} = '';
        set(h{m}, 'yticklabel', this_label);
        subplot(h{m});
        if m <= beta_len
            add_means(beta_all_samples(m, :), this_y_max);
        else
            add_means(y_bar_all_samples, this_y_max);
        end
    end
    
    h_subplots = [h{:}];
    
    function add_means(data, y_max)
        hold on;
        clr = lines(7);
        if get_argument({'plot_mean', 'plotmean'}, true, varargin{:});
            plot(ones(2, 1) * mean(data), [0 y_max], 'LineWidth', 1.5, 'Color', clr(7, :))
        end
        if get_argument({'plot_median', 'plotmedian'}, false, varargin{:});
            plot(ones(2, 1) * median(data), [0 y_max], 'LineWidth', 1.5, 'Color', clr(5, :))
        end
        if get_argument({'plot_mode', 'plotmode'}, false, varargin{:});
            plot(ones(2, 1) * half_sample_mode(data), [0 y_max], 'LineWidth', 1.5, 'Color', clr(6, :))
        end
        hold off;
    end
end
