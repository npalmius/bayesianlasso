function [predictions, beta_sampled, sigma_squared_sampled, lambda_beta_sampled] = bayesian_lasso_normalised(X, y, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-Sep-2016

    close_all_figures(varargin{:})
    
    assert(size(X, 1) == numel(y), 'X must have the same number of rows as there are elements as y.');
    
    max_training_elements = get_argument('max_training_elements', [], varargin{:});
    
    % Set up parameters
    lambda_beta = get_argument('lambda_beta', 1, varargin{:});
    sweeps = get_argument('sweeps', 500, varargin{:});
    
    % Initialise data variables
    n_full = size(X, 1);
    p = size(X, 2);

    if ~isempty(max_training_elements)
        n_train = min(n_full, max_training_elements);
    else
        n_train = n_full;
    end

    X_train = X(1:n_train, :);
    y_train = y(1:n_train);

    X_train_mean = mean(X_train, 1);
    X_train_std = std(X_train, 1);
    
    X_norm = (X - repmat(X_train_mean, n_full, 1)) ./repmat(X_train_std, n_full, 1);
    X_train_norm = (X_train - repmat(X_train_mean, n_train, 1)) ./repmat(X_train_std, n_train, 1);
    
    y_train_mean = mean(y_train);
    y_train_norm = y_train - y_train_mean;

    % Initialise model variables
    sigma_squared = 1;
    tau_beta_squared = ones(p, 1);

    % Initialise output variables
    predictions = NaN(n_full, sweeps);
    beta_sampled = NaN(p, sweeps);
    sigma_squared_sampled = NaN(1, sweeps);
    lambda_beta_sampled = NaN(1, sweeps);

    new_figure(varargin{:});
    
    for sw = 1:sweeps
        fprintf('Sweep %i of %i (%.1f%%)\n', sw, sweeps, (sw / sweeps) * 100);

        [sigma_squared, tau_beta_squared, beta, lambda_beta] = bayesian_lasso_sweep_normalised(sigma_squared, tau_beta_squared, lambda_beta, X_train_norm, y_train_norm, varargin{:});

        beta_sampled(:, sw) = beta;
        sigma_squared_sampled(sw) = sigma_squared;
        lambda_beta_sampled(sw) = lambda_beta;

        predictions(:, sw) = X_norm * beta + y_train_mean;

        plot_predictions(y, predictions, varargin{:});

        pause(0.1);
    end
end
