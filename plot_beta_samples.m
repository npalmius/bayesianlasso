function [] = plot_beta_samples(beta_samples, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Sep-2016

    if ~get_argument({'show_figures', 'showfigures', 'show_figure', 'showfigure'}, true, varargin{:});
        return
    end
    
    beta_max_min = max([ceil(max(max(beta_samples))) -floor(min(min(beta_samples)))]);
    beta_max = beta_max_min;
    beta_min = -beta_max_min;
    beta_range = linspace(beta_min, beta_max, 200);
    beta_len = size(beta_samples, 1);

    beta_hist = NaN(beta_len, numel(beta_range));

    if size(beta_samples, 2) > 1
        for m = 1:beta_len
            beta_hist(m, :) = histc(beta_samples(m, :), beta_range);
        end
    end

    imagesc(beta_range, 1:beta_len, beta_hist);
    clr = flipud(autumn);
    clr(1, :) = 1;
    colormap(clr);
    set(gca, 'YDir', 'normal')
    
    clr = lines(7);

    hold on;
    if get_argument({'plot_mean', 'plotmean'}, true, varargin{:});
        scatter(nanmean(beta_samples, 2), 1:beta_len, 200, 'x', 'LineWidth', 1.5, 'MarkerEdgeColor', clr(1, :))
    end
    if get_argument({'plot_median', 'plotmedian'}, false, varargin{:});
        scatter(nanmedian(beta_samples, 2), 1:beta_len, 200, '^', 'LineWidth', 1.5, 'MarkerEdgeColor', clr(7, :))
    end
    if get_argument({'plot_mode', 'plotmode'}, false, varargin{:});
        [~, mode_i] = max(beta_hist, [], 2);
        scatter(beta_range(mode_i), 1:beta_len, 200, '+', 'LineWidth', 1.5, 'MarkerEdgeColor', clr(6, :))
    end
    hold off;
    
    xlabel('\beta')
    ylabel('Feature')
end
