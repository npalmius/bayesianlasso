function [sigma_squared, tau_beta_squared, beta, lambda_beta] = bayesian_lasso_sweep_normalised(sigma_squared_in, tau_beta_squared_in, lambda_beta_in, X, y, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-Sep-2016

    %% Set up parameters
    sample_lambda_beta = get_argument('sample_lambda_beta', false, varargin{:});
    em_optimise_lambda_beta = get_argument('em_optimise_lambda_beta', false, varargin{:});
    assert(~(sample_lambda_beta && em_optimise_lambda_beta), 'Lambda beta can be either sampled or EM optimised.');
    
    if sample_lambda_beta
        lambda_beta_squared_delta = get_argument({'lambda_beta_squared_delta', 'lambda_beta_delta'}, 1, varargin{:});
        lambda_beta_squared_r = get_argument({'lambda_beta_squared_r', 'lambda_beta_r'}, 1, varargin{:});
    elseif em_optimise_lambda_beta
        lambda_beta_squared_delta = get_argument({'lambda_beta_squared_delta', 'lambda_beta_delta'}, 0, varargin{:});
        lambda_beta_squared_r = get_argument({'lambda_beta_squared_r', 'lambda_beta_r'}, 1, varargin{:});
    end

    % Parameters of sigma_squared prior.
    sigma_squared_a = get_argument('sigma_squared_a', 0, varargin{:});
    sigma_squared_gamma = get_argument('sigma_squared_gamma', 0, varargin{:});

    %% Sample variables
    beta = sample_beta(sigma_squared_in, tau_beta_squared_in, X, y);

    sigma_squared = sample_sigma_squared(sigma_squared_a, sigma_squared_gamma, tau_beta_squared_in, beta, X, y);

    tau_beta_squared = sample_tau_beta_squared(lambda_beta_in, beta, sigma_squared);
    
    if sample_lambda_beta
        lambda_beta = sqrt(sample_lambda_beta_squared(lambda_beta_squared_delta, lambda_beta_squared_r, tau_beta_squared));
    elseif em_optimise_lambda_beta
        lambda_beta = sqrt(2 * (numel(beta) + lambda_beta_squared_r - 1) / (sum(tau_beta_squared) + (2 * lambda_beta_squared_delta)));
    else
        lambda_beta = lambda_beta_in;
    end

    function sample = sample_tau_beta_squared(lambda_beta, beta, sigma_squared)
        tau_mu = sqrt(((lambda_beta ^ 2) * sigma_squared) ./ (beta .^ 2));
        tau_lambda = lambda_beta ^ 2;

        sample = NaN(numel(beta), 1);
        
        for m = 1:numel(beta)
            pd = makedist('InverseGaussian', 'mu', tau_mu(m), 'lambda', tau_lambda);

            sample_inverse = random(pd, 1);

            sample(m) = 1 / sample_inverse;
        end
    end

    function sample = sample_beta(sigma_squared, tau_beta_squared, X, y)
        D_beta = diag(tau_beta_squared);
        
        A = (X' * X) + inv(D_beta);

        beta_mvn_mu = A \ X' * y;
        beta_mvn_sigma = sigma_squared * inv(A); %#ok<MINV>

        sample = mvnrnd(beta_mvn_mu, beta_mvn_sigma)';
    end

    function sample = sample_sigma_squared(sigma_squared_a, sigma_squared_gamma, tau_beta_squared, beta, X, y)
        D_beta = diag(tau_beta_squared);
        
        sigma_square_shape = ((numel(y) - 1) / 2) + (numel(beta) / 2) + sigma_squared_a;
        sigma_square_scale = (((y - (X * beta))' * (y - (X * beta))) / 2) + (((beta' / D_beta) * beta) / 2) + sigma_squared_gamma;

        sample = 1 / gamrnd(sigma_square_shape, 1 / sigma_square_scale);
    end

    function sample = sample_lambda_beta_squared(lambda_beta_squared_delta, lambda_beta_squared_r, tau_beta_squared)
        lambda_squared_shape = numel(tau_beta_squared) + lambda_beta_squared_r;
        lambda_squared_rate = (sum(lambda_beta_squared_r) / 2) + lambda_beta_squared_delta;

        sample = gamrnd(lambda_squared_shape, 1 / lambda_squared_rate);
    end
end
