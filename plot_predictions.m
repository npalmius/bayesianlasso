function [] = plot_predictions(y, predictions, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-Sep-2016

    if ~get_argument({'show_figures', 'showfigures', 'show_figure', 'showfigure'}, true, varargin{:});
        return
    end
    
    burnin = get_argument({'burn_in', 'burnin'}, 0, varargin{:});
    y_rounding_factor = get_argument({'y_rounding_factor', 'yroundingfactor'}, 5, varargin{:});
    
    y_max = max([max(predictions(:, (burnin + 1):end), [], 2); y]);
    y_min = min([min(predictions(:, (burnin + 1):end), [], 2); y]);
    
    if y_rounding_factor > 0
        y_max = ceil(y_max / y_rounding_factor) * y_rounding_factor;
        y_min = floor(y_min / y_rounding_factor) * y_rounding_factor;
    end
    
    y_max = get_argument({'y_max', 'ymax'}, y_max, varargin{:});
    y_min = get_argument({'y_min', 'ymin'}, y_min, varargin{:});
    
    y_range = linspace(y_min, y_max, 100);
    y_len = numel(y);

    y_hist = NaN(y_len, numel(y_range));

    if size(predictions, 2) > 1
        for m=1:y_len
            y_hist(m, :) = histc(predictions(m, (burnin + 1):end), y_range);
        end
    end

    imagesc(1:y_len, y_range, y_hist');
    clr = flipud(autumn);
    clr(1, :) = 1;
    colormap(clr);
    set(gca, 'YDir', 'normal')

    clr = lines(7);

    hold on;
    scatter(1:y_len, y, 100, 'o', 'LineWidth', 1.5, 'MarkerEdgeColor', clr(1, :))
    if get_argument({'plot_mean', 'plotmean'}, true, varargin{:}) && (size(predictions, 2) > 1)
        scatter(1:y_len, nanmean(predictions(:, (burnin + 1):end), 2), 200, 'x', 'LineWidth', 1.5, 'MarkerEdgeColor', clr(4, :))
    end
    hold off;
    
    xlabel(get_argument({'x_label', 'xlabel'}, 'Data point', varargin{:}));
    ylabel(get_argument({'y_label', 'ylabel'}, 'Value', varargin{:}));
end
