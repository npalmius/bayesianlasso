function [predictions, beta, properties] = linear_lasso_normalised(X, y, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Sep-2016

    assert(size(X, 1) == numel(y), 'X must have the same number of rows as there are elements as y.');
    
    max_training_elements = get_argument('max_training_elements', [], varargin{:});
    
    % Initialise data variables
    n_full = size(X, 1);
    p = size(X, 2);

    if ~isempty(max_training_elements)
        n_train = min(n_full, max_training_elements);
    else
        n_train = n_full;
    end

    X_train = X(1:n_train, :);
    y_train = y(1:n_train);

    X_train_mean = mean(X_train, 1);
    X_train_std = std(X_train, 1);
    
    X_norm = (X - repmat(X_train_mean, n_full, 1)) ./repmat(X_train_std, n_full, 1);
    X_train_norm = (X_train - repmat(X_train_mean, n_train, 1)) ./repmat(X_train_std, n_train, 1);
    
    y_train_mean = mean(y_train);
    y_train_norm = y_train - y_train_mean;

    glmopts_dist = 'normal';
    glmopts_link = 'identity';
    
    [B_lasso, FI_lasso] = lassoglm(X_train_norm, y_train_norm, glmopts_dist, 'CV', 10);
    model_fitted = [FI_lasso.Intercept; B_lasso];

    predictions = NaN(n_full, numel(FI_lasso.Lambda));
    beta = NaN(p, numel(FI_lasso.Lambda));
    
    for i = 1:numel(FI_lasso.Lambda)
        this_model_fitted = model_fitted(:, i);

        predictions(:, i) = glmval(this_model_fitted, X_norm, glmopts_link) + y_train_mean;
        beta(:, i) = this_model_fitted(2:end);
    end
    
    properties = struct();
    properties.Lambda = FI_lasso.Lambda;
    properties.IndexMinDeviance = FI_lasso.IndexMinDeviance;
    properties.Index1SE = FI_lasso.Index1SE;
end
