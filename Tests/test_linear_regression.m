function test_linear_regression(X, y, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 11-Sep-2016

    OUTPUT_SUB_DIR = 'bayesian_lasso/test/linear_regression';
    
    close_all_figures(varargin{:})
    
    assert(size(X, 1) == numel(y), 'X must have the same number of rows as there are elements as y.');
    
    max_training_elements = get_argument('max_training_elements', [], varargin{:});
    
    % Initialise data variables
    n_full = size(X, 1);

    if ~isempty(max_training_elements)
        n_train = min(n_full, max_training_elements);
    else
        n_train = n_full;
    end

    [prediction, beta] = linear_regression_normalised(X, y, varargin{:});
    
    h = figure;
    plot_predictions(y, prediction, varargin{:});
    set(gca, 'ylim', [0 30]);
    title('Linear Regression');
    save_figure('predictions_linear_regression', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    plot_beta_samples(beta, varargin{:});
    title('Linear Regression');
    save_figure('beta_linear_regression', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
        
    error_train = mean(abs(prediction(1:n_train) - y(1:n_train)));
    
    fprintf('Training set MAE: %.4f\n', error_train);
    
    if n_train < n_full
        error_test = mean(abs(prediction((n_train+1):end) - y((n_train+1):end)));
        
        fprintf('Test set MAE: %.4f\n', error_test);
    else
        fprintf('Test set MAE: N/A\n');
    end
end
