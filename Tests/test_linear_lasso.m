function test_linear_lasso(X, y, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 06-Sep-2016

    OUTPUT_SUB_DIR = 'bayesian_lasso/test/linear_lasso';
    
    close_all_figures(varargin{:})
    
    assert(size(X, 1) == numel(y), 'X must have the same number of rows as there are elements as y.');
    
    max_training_elements = get_argument('max_training_elements', [], varargin{:});
    
    % Initialise data variables
    n_full = size(X, 1);

    if ~isempty(max_training_elements)
        n_train = min(n_full, max_training_elements);
    else
        n_train = n_full;
    end

    [predictions, beta, properties] = linear_lasso_normalised(X, y, varargin{:});
    
    h = figure;
    plot_predictions(y, predictions(:, properties.IndexMinDeviance), varargin{:});
    set(gca, 'ylim', [0 30]);
    title('Linear Lasso (Min CV Deviance)');
    save_figure('predictions_linear_lasso_min_deviance', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    plot_predictions(y, predictions(:, properties.Index1SE), varargin{:});
    set(gca, 'ylim', [0 30]);
    title('Linear Lasso (Min CV Deviance + 1SE)');
    save_figure('predictions_linear_lasso_min_deviance_1_SE', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    plot_predictions(y, predictions, varargin{:});
    set(gca, 'ylim', [0 30]);
    title('Linear Lasso (All)');
    save_figure('predictions_linear_lasso_all', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    plot_beta_samples(beta(:, properties.IndexMinDeviance), varargin{:});
    title('Linear Lasso (Min CV Deviance)');
    save_figure('beta_linear_lasso_min_deviance', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    plot_beta_samples(beta(:, properties.Index1SE), varargin{:});
    title('Linear Lasso (Min CV Deviance + 1SE)');
    save_figure('beta_linear_lasso_min_deviance_1_SE', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    plot_beta_samples(beta, varargin{:});
    title('Linear Lasso (All)');
    save_figure('beta_linear_lasso_all', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    if n_train < n_full
        error_train = NaN(numel(properties.Lambda), 1);
        error_test = NaN(numel(properties.Lambda), 1);
        
        for i = 1:numel(properties.Lambda)
            error_train(i) = mean(abs(predictions(1:n_train, i) - y(1:n_train)));
            error_test(i) = mean(abs(predictions((n_train+1):end, i) - y((n_train+1):end)));
        end
        
        result_valid = true(1, numel(properties.Lambda));
        result_valid(properties.IndexMinDeviance) = false;
        result_valid(properties.Index1SE) = false;
        
        h = figure;
        
        scatter(error_train(result_valid), error_test(result_valid), 100, 'x', 'LineWidth', 1.5);
        
        hold on
        h1 = scatter(error_train(properties.IndexMinDeviance), error_test(properties.IndexMinDeviance), 100, '+', 'LineWidth', 1.5);
        h2 = scatter(error_train(properties.Index1SE), error_test(properties.Index1SE), 100, '+', 'LineWidth', 1.5);
        
        prediction = nanmean(predictions, 2);
        
        error_train = mean(abs(prediction(1:n_train) - y(1:n_train)));
        error_test = mean(abs(prediction((n_train+1):end) - y((n_train+1):end)));
        
        h3 = scatter(error_train, error_test, 100, 's', 'LineWidth', 1.5);

        prediction = linear_regression_normalised(X, y, varargin{:});
        
        error_train = mean(abs(prediction(1:n_train) - y(1:n_train)));
        error_test = mean(abs(prediction((n_train+1):end) - y((n_train+1):end)));
        
        h4 = scatter(error_train, error_test, 100, '*', 'LineWidth', 1.5);
        hold off
        
        legend([h1; h2; h3; h4], 'Min CV Deviance', 'Min CV Deviance + 1SE', 'CV Mean', 'Linear Regression')

        xlabel('Training set MAE');
        ylabel('Test set MAE');

        save_figure('error_train_test', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
    end
end
