function [] = test_linear_bayesian_lasso(X, y, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 06-Sep-2016

    OUTPUT_SUB_DIR = 'bayesian_lasso/test/linear_bayesian_lasso';
    
    close_all_figures(varargin{:})
    
    assert(size(X, 1) == numel(y), 'X must have the same number of rows as there are elements as y.');
    
    max_training_elements = get_argument('max_training_elements', [], varargin{:});
    
    % Initialise data variables
    n_full = size(X, 1);

    if ~isempty(max_training_elements)
        n_train = min(n_full, max_training_elements);
    else
        n_train = n_full;
    end

    lambda_range = [0.1:0.1:0.9 1:0.25:10 11:0.5:15 16:20];
    
    error_train = NaN(numel(lambda_range), 1);
    error_test = NaN(numel(lambda_range), 1);

    for l = 1:numel(lambda_range)
        fprintf('%s = %.2f\n', char(955), lambda_range(l))
        
        [predictions, beta_sampled_all, sigma_squared_sampled_all] = bayesian_lasso_normalised(X, y, 'lambda_beta', lambda_range(l), 'close_figures', false, 'show_figures', false, 'sample_lambda_beta', false, 'em_optimise_lambda_beta', false, 'sweeps', 1000, varargin{:});
        
        h = figure;
        plot_predictions(y, predictions, varargin{:});
        set(gca, 'ylim', [0 30]);
        title(sprintf('\\lambda = %.3f', lambda_range(l)));
        save_figure(strrep(sprintf('predictions_lambda_%.3f', lambda_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_beta_samples(beta_sampled_all, varargin{:});
        title(sprintf('\\lambda = %.3f', lambda_range(l)));
        save_figure(strrep(sprintf('beta_lambda_%.3f', lambda_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_sigma_squared_samples(sigma_squared_sampled_all, varargin{:});
        title(sprintf('\\lambda = %.3f', lambda_range(l)));
        save_figure(strrep(sprintf('sigma_squared_lambda_%.3f', lambda_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        prediction = nanmean(predictions, 2);
        
        if n_train < n_full
            error_train(l) = mean(abs(prediction(1:n_train) - y(1:n_train)));
            error_test(l) = mean(abs(prediction((n_train+1):end) - y((n_train+1):end)));
        end
    end
    
    lambda_beta_squared_delta_range = [0 1/1000 1/100 1/10 0.25:0.25:1 2:2:10 100 1000];
    
    lambda_beta_sampled_mean = NaN(numel(lambda_beta_squared_delta_range), 1);
    lambda_beta_sampled_median = NaN(numel(lambda_beta_squared_delta_range), 1);
    error_train_sampled = NaN(numel(lambda_beta_squared_delta_range), 1);
    error_test_sampled = NaN(numel(lambda_beta_squared_delta_range), 1);
    
    lambda_beta_em_optimised_mean = NaN(numel(lambda_beta_squared_delta_range), 1);
    lambda_beta_em_optimised_median = NaN(numel(lambda_beta_squared_delta_range), 1);
    error_train_em_optimised = NaN(numel(lambda_beta_squared_delta_range), 1);
    error_test_em_optimised = NaN(numel(lambda_beta_squared_delta_range), 1);
    
    for l = 1:numel(lambda_beta_squared_delta_range)
        fprintf('%s = sampled, %s = %.1f\n', char(955), char(948), lambda_beta_squared_delta_range(l))

        [predictions, beta_sampled_all, sigma_squared_sampled_all, lambda_beta_sampled_all] = bayesian_lasso_normalised(X, y, 'lambda_beta', 1, 'close_figures', false, 'show_figures', false, 'sample_lambda_beta', true, 'lambda_beta_squared_delta', lambda_beta_squared_delta_range(l), 'sweeps', 1000, varargin{:});

        h = figure;
        plot_predictions(y, predictions, varargin{:});
        set(gca, 'ylim', [0 30]);
        title(sprintf('Sampled \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('predictions_lambda_sampled_%.3f', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_beta_samples(beta_sampled_all, varargin{:});
        title(sprintf('Sampled \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('beta_lambda_sampled_%.3f', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_sigma_squared_samples(sigma_squared_sampled_all, varargin{:});
        title(sprintf('Sampled \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('sigma_squared_lambda_sampled_%.3f', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_lambda_samples(lambda_beta_sampled_all, varargin{:});
        title(sprintf('Sampled \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('lambda_sampled_%.3f', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_lambda_samples_time(lambda_beta_sampled_all, varargin{:});
        title(sprintf('Sampled \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('lambda_sampled_%.3f_time', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        prediction = nanmean(predictions, 2);

        if n_train < n_full
            lambda_beta_sampled_mean(l) = mean(lambda_beta_sampled_all);
            lambda_beta_sampled_median(l) = median(lambda_beta_sampled_all);
            error_train_sampled(l) = mean(abs(prediction(1:n_train) - y(1:n_train)));
            error_test_sampled(l) = mean(abs(prediction((n_train+1):end) - y((n_train+1):end)));
        end

        [predictions, beta_em_optimised_all, sigma_squared_em_optimised_all, lambda_beta_em_optimised_all] = bayesian_lasso_normalised(X, y, 'lambda_beta', 1, 'close_figures', false, 'show_figures', false, 'em_optimise_lambda_beta', true, 'lambda_beta_squared_delta', lambda_beta_squared_delta_range(l), 'sweeps', 1000, varargin{:});

        h = figure;
        plot_predictions(y, predictions, varargin{:});
        set(gca, 'ylim', [0 30]);
        title(sprintf('EM Optimised \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('predictions_lambda_em_optimised_%.3f', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_beta_samples(beta_em_optimised_all, varargin{:});
        title(sprintf('EM Optimised \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('beta_lambda_em_optimised_%.3f', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_sigma_squared_samples(sigma_squared_em_optimised_all, varargin{:});
        title(sprintf('EM Optimised \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('sigma_squared_lambda_em_optimised_%.3f', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_lambda_samples(lambda_beta_em_optimised_all, varargin{:});
        title(sprintf('EM Optimised \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('lambda_em_optimised_%.3f', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        h = figure;
        plot_lambda_samples_time(lambda_beta_em_optimised_all, varargin{:});
        title(sprintf('EM Optimised \\lambda, \\delta = %.3f', lambda_beta_squared_delta_range(l)));
        save_figure(strrep(sprintf('lambda_em_optimised_%.3f_time', lambda_beta_squared_delta_range(l)), '.', '_'), 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
        close(h)
        
        prediction = nanmean(predictions, 2);

        if n_train < n_full
            lambda_beta_em_optimised_mean(l) = mean(lambda_beta_em_optimised_all);
            lambda_beta_em_optimised_median(l) = median(lambda_beta_em_optimised_all);
            error_train_em_optimised(l) = mean(abs(prediction(1:n_train) - y(1:n_train)));
            error_test_em_optimised(l) = mean(abs(prediction((n_train+1):end) - y((n_train+1):end)));
        end
    end
    
    h = figure;
    scatter(error_train, error_test, 100, 'x', 'LineWidth', 1.5)
    hold on;
    scatter(error_train_sampled, error_test_sampled, 100, '+', 'LineWidth', 1.5)
    scatter(error_train_em_optimised, error_test_em_optimised, 100, '*', 'LineWidth', 1.5)
    hold off
    xlabel('Training set MAE');
    ylabel('Test set MAE');
    legend('Fixed \lambda', 'Sampled \lambda', 'EM Optimised \lambda', 'Location', 'NorthEast');
    save_figure('error_train_test', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    scatter(lambda_range, error_train, 100, 'x', 'LineWidth', 1.5)
    hold on;
    scatter(lambda_beta_sampled_mean, error_train_sampled, 100, '+', 'LineWidth', 1.5)
    scatter(lambda_beta_em_optimised_mean, error_train_em_optimised, 100, '*', 'LineWidth', 1.5)
    hold off
    xlabel('\lambda');
    ylabel('Training set MAE');
    legend('Fixed \lambda', 'Sampled \lambda', 'EM Optimised \lambda', 'Location', 'SouthEast');
    save_figure('error_lambda_train', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    scatter(lambda_range, error_test, 100, 'x', 'LineWidth', 1.5)
    hold on;
    scatter(lambda_beta_sampled_mean, error_test_sampled, 100, '+', 'LineWidth', 1.5)
    scatter(lambda_beta_em_optimised_mean, error_test_em_optimised, 100, '*', 'LineWidth', 1.5)
    hold off
    xlabel('\lambda');
    ylabel('Test set MAE');
    legend('Fixed \lambda', 'Sampled \lambda', 'EM Optimised \lambda', 'Location', 'NorthEast');
    save_figure('error_lambda_test', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
    
    h = figure;
    scatter(lambda_beta_squared_delta_range, lambda_beta_sampled_mean, 100, 'x', 'LineWidth', 1.5)
    hold on;
    scatter(lambda_beta_squared_delta_range, lambda_beta_sampled_median, 100, '+', 'LineWidth', 1.5)
    scatter(lambda_beta_squared_delta_range, lambda_beta_em_optimised_mean, 100, 'x', 'LineWidth', 1.5)
    scatter(lambda_beta_squared_delta_range, lambda_beta_em_optimised_median, 100, '+', 'LineWidth', 1.5)
    hold off
    xlabel('\delta');
    ylabel('\lambda');
    h_legend = legend('Sampled \lambda mean', 'Sampled \lambda median', 'EM Optimised \lambda mean', 'EM Optimised \lambda median', 'Location', 'NorthEast');
    save_figure('sampled_lambda_delta', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    set(gca,'XScale','log');
    save_figure('sampled_lambda_delta_log', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    set(gca,'YScale','log');
    set(h_legend, 'Location', 'SouthWest')
    save_figure('sampled_lambda_delta_log_log', 'output_sub_dir', OUTPUT_SUB_DIR, varargin{:});
    close(h)
end
